﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Input;
using Telerik.Windows.Controls;

namespace Autoscrolling_WPF
{
    public class SetPositionToShapesCommand : ICommand
    {
        private readonly bool _small;

        public SetPositionToShapesCommand(bool small)
        {
            _small = small;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            var radDiagram = parameter as RadDiagram;
            if (radDiagram == null) return;

            if (_small)
                SetShapePositionForSmallDiagram(radDiagram);
            else
                SetShapePositionForBigDiagram(radDiagram);
        }

        private static void SetShapePositionForSmallDiagram(RadDiagram radDiagram)
        {
            radDiagram.Shapes[0].Position = new Point(200, 160);
            radDiagram.Shapes[1].Position = new Point(200, 260);
            radDiagram.Shapes[2].Position = new Point(300, 160);
        }

        private static void SetShapePositionForBigDiagram(RadDiagram radDiagram)
        {
            radDiagram.Shapes[0].Position = new Point(200, 160);
            radDiagram.Shapes[1].Position = new Point(6200, 260);
            radDiagram.Shapes[2].Position = new Point(300, 6150);
        }

        public event EventHandler CanExecuteChanged;
    }
}