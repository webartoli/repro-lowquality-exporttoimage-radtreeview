﻿using System.Windows;
using System.Windows.Input;

namespace Autoscrolling_WPF
{
    public partial class MainWindow : Window
    {
        public ICommand ExportToImageTelerikCommand { get; set; }
        public ICommand ExportToImageShawnCommand { get; set; }
        public ICommand SetPositionToShapesSimulatingSmallDiagramCommand { get; set; }
        public ICommand SetPositionToShapesSimulatingBigDiagramCommand { get; set; }

        public MainWindow()
        {
            InitializeComponent();

            DataContext = this;
            ExportToImageTelerikCommand = new ExportToImageTelerikCommand();
            ExportToImageShawnCommand = new ExportToImageShawnCommand();
            SetPositionToShapesSimulatingSmallDiagramCommand = new SetPositionToShapesCommand(true);
            SetPositionToShapesSimulatingBigDiagramCommand = new SetPositionToShapesCommand(false);
        }
    }
}
