﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Media.Imaging;
using Telerik.Windows.Controls;
using Telerik.Windows.Diagrams.Core;
using ICommand = System.Windows.Input.ICommand;

namespace Autoscrolling_WPF
{
    public class ExportToImageTelerikCommand : ICommand
    {
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            var radDiagram = parameter as RadDiagram;
            
            if (radDiagram == null) return;
            
            // Prepare image
            var encoder = new PngBitmapEncoder();
            var enclosingBounds = radDiagram.CalculateEnclosingBounds();
            var size = enclosingBounds.Size;
            var margin = default(Thickness);
            
            // Save Image
            using (var stream = File.Open(GetTempFilename(), FileMode.Create))
            {
                radDiagram.ExportToImage(stream, encoder,enclosingBounds,size,null,margin);
            }
        }

        private static string GetTempFilename()
        {
            return $"{Path.GetFileNameWithoutExtension(Path.GetTempFileName())}.png";
        }

        public event EventHandler CanExecuteChanged;
    }
}