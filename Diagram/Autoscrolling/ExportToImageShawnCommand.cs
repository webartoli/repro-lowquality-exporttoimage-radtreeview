using System;
using System.IO;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Telerik.Windows.Controls;
using IGraphExtensions = Telerik.Windows.Diagrams.Core.IGraphExtensions;

namespace Autoscrolling_WPF
{
    public class ExportToImageShawnCommand : ICommand
    {
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            var radDiagram = parameter as RadDiagram;
            
            if (radDiagram == null) return;
            
            // Prepare image
            var enclosingBounds = IGraphExtensions.CalculateEnclosingBounds(radDiagram);
            
            // Test 1
            CopyImagetoFile(radDiagram, enclosingBounds, GetTempFilename());
        }

        private static string GetTempFilename()
        {
            return $"{Path.GetFileNameWithoutExtension(Path.GetTempFileName())}.png";
        }

        private void CopyImagetoFile(FrameworkElement surface, Rect rectangle, string fullName, double dpi = 96d)
        {
            // Now scale the layout to fit the bitmap
            var renderBitmap =
                new RenderTargetBitmap(
                    (int)rectangle.Width,
                    (int)rectangle.Height,
                    dpi,
                    dpi,
                    PixelFormats.Default);

            var dv = new DrawingVisual();

            using (DrawingContext dc = dv.RenderOpen())
            {
                var vb = new BitmapCacheBrush(surface)
                {
                    BitmapCache = new BitmapCache { SnapsToDevicePixels = true, EnableClearType = true }
                };
                dc.DrawRectangle(vb, null, rectangle);
            }

            renderBitmap.Render(dv);
            using (var outStream = new FileStream(fullName, FileMode.Create))
            {
                // Use png encoder for our data
                var encoder = new PngBitmapEncoder();
                // push the rendered bitmap to it
                encoder.Frames.Add(BitmapFrame.Create(renderBitmap));
                // save the data to the stream
                encoder.Save(outStream);
            }
        }

        public event EventHandler CanExecuteChanged;
    }
}